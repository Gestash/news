package com.example.news

import android.app.Application
import com.example.news.di.DaggerSingletonComponent
import com.example.news.di.SingletonComponent
import com.example.news.di.module.ContextModule

class NewsApplication: Application() {
    lateinit var comp: SingletonComponent

    override fun onCreate() {
        super.onCreate()
        instance = this
        comp = DaggerSingletonComponent
            .builder()
            .contextModule(ContextModule(this))
            .build()
    }

    companion object {
        private var instance: NewsApplication? = null

        @JvmStatic
        fun getComponent() : SingletonComponent {
            return instance!!.comp
        }
    }
}