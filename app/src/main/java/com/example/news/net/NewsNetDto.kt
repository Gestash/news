package com.example.news.net

import com.google.gson.annotations.SerializedName

data class NewsNetDto(
    val topic: NewsTopicNetDto
)

data class NewsTopicNetDto(
    val headline: NewsHeadlineNetDto
)

data class NewsHeadlineNetDto(
    val type: String,
    val info: NewsInfoNetDto,
    @SerializedName("title_image")
    val titleImage: NewsImageNetDto
)

data class NewsInfoNetDto(
    val id: String,
    val title: String,
    val announce: String
)

data class NewsImageNetDto(
    val url: String
)