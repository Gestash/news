package com.example.news.net

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface NewsService {

    @GET("lists/latest")
    @Headers("Accept: application/json")
    fun getLatestEndpoints(): Call<LatestNewsListNetDto>

    @GET("{endPoint}")
    @Headers("Accept: application/json","X-Lenta-Media-Type: 1")
    fun getNews(@Path("endPoint", encoded = true) endPoint:String):Call<NewsNetDto>
}