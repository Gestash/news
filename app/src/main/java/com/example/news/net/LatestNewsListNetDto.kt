package com.example.news.net

data class LatestNewsListNetDto(
    val headlines: List<LatestNewsNetDto>
)

data class LatestNewsNetDto(
    val type: String,
    val info: LatestNewsInfoNetDto
)
data class LatestNewsInfoNetDto(
    val id: String
)