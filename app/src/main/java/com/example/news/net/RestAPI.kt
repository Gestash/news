package com.example.news.net

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.IOException


class RestAPI(private val service: NewsService) {

    suspend fun getLatestEndpoints(): LatestNewsListNetDto? =
        withContext(Dispatchers.IO) {

            val call = service.getLatestEndpoints()

            try {
                call.execute().body()
            } catch (e: IOException) {
                null
            }
        }

    suspend fun getNews(endpoint: String): NewsNetDto? =
        withContext(Dispatchers.IO) {
            val call = service.getNews(endpoint)
            try {
                call.execute().body()
            } catch (e: IOException) {
                null
            }
        }


}