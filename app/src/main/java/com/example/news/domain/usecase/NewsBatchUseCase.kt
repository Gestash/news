package com.example.news.domain.usecase

import com.example.news.domain.datasource.EndpointDataSource
import com.example.news.domain.datasource.NewsDataSource
import com.example.news.domain.entity.Endpoint
import com.example.news.domain.entity.News
import com.example.news.domain.factory.UiFactory
import com.example.news.ui.NewsUiDto

class NewsBatchUseCase(
    private val endpointDataSource: EndpointDataSource,
    private val newsDataSource: NewsDataSource
) {

    private val uiFactory = UiFactory()

    private val limit = 5

    suspend fun loadNewsBatch(offset: Int): List<NewsUiDto> {

        val endpoints = endpointDataSource.getEndpointsFromDb(limit, offset)
        val ids = endpoints.mapNotNull { it.id }

        val dbNews = newsDataSource.getNewsFromDb(ids)

        if (dbNews.count() == limit) {
            return dbNews.sortedByDescending { it.newsLinksId }.map { uiFactory.createNewsUiDto(it) }
        }
        val endpointsToLoad = filterNotLoadedEndpoints(dbNews, endpoints)

        val netNews = newsDataSource.getNewsFromNet(endpointsToLoad)

        newsDataSource.saveNewsToDb(netNews)

        return listOf(dbNews, netNews).flatten().sortedByDescending { it.newsLinksId }.map { uiFactory.createNewsUiDto(it) }
    }

    private fun filterNotLoadedEndpoints(
        news: List<News>,
        endpoints: List<Endpoint>
    ): List<Endpoint> {
        val ids = news.map { it.newsLinksId }
        return endpoints.filterNot { ids.contains(it.id) }
    }

    suspend fun loadNewsBatchFromDb(offset: Int): List<NewsUiDto> {

        val dbNews = newsDataSource.getLastNewsFromDb(limit, offset)

        return dbNews.map { uiFactory.createNewsUiDto(it) }

    }
}