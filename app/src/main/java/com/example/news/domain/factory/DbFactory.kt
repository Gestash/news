package com.example.news.domain.factory

import com.example.news.db.dto.NewsDbDto
import com.example.news.db.dto.NewsEndpointDbDto
import com.example.news.domain.entity.Endpoint
import com.example.news.domain.entity.News

class DbFactory {

    fun createLatestNewsEndpoint(dto: NewsEndpointDbDto): Endpoint {
        val endpoint = dto.endpoint
        val id = dto.id

        return Endpoint(id, endpoint)
    }

    fun createNews(dto: NewsDbDto): News {

        val newsLinksId = dto.newsLinksId
        val title = dto.title
        val text = dto.text
        val imageUrl = dto.imageUrl

        return News(newsLinksId, title, text, imageUrl)
    }

    fun createNewsEndpointDbDto(entity: Endpoint): NewsEndpointDbDto {

        val endpoint = entity.endpoint

        return NewsEndpointDbDto(endpoint)
    }

    fun createNewsDbDto(entity: News): NewsDbDto {

        val newsLinksId = entity.newsLinksId
        val title = entity.title
        val text = entity.text
        val imageUrl = entity.imageUrl

        return NewsDbDto(newsLinksId, title, text, imageUrl)
    }
}