package com.example.news.domain.datasource

import com.example.news.db.repository.EndpointsRepository
import com.example.news.domain.entity.Endpoint
import com.example.news.domain.factory.DbFactory
import com.example.news.domain.factory.NetFactory
import com.example.news.net.RestAPI

class EndpointDataSource(
    private val restAPI: RestAPI,
    private val endpointsRepository: EndpointsRepository

) {
    private val netFactory = NetFactory()
    private val dbFactory = DbFactory()

    suspend fun saveEndpointsToDb(endpoints: List<Endpoint>) {
        val endpointsToDb = endpoints.map { dbFactory.createNewsEndpointDbDto(it) }
        endpointsRepository.insertAll(endpointsToDb)
    }

    suspend fun getEndpointsFromDb(limit: Int, offset: Int): List<Endpoint> {
        val dbEndpoints = endpointsRepository.selectLastBatch(limit, offset)
        return dbEndpoints.map { dbFactory.createLatestNewsEndpoint(it) }
    }

    suspend fun getLatestEndpointsFromNet(): List<Endpoint>? {
        val endpointNet = restAPI.getLatestEndpoints() ?: return null
        return endpointNet.headlines.map { netFactory.createLatestNewsEndpoint(it) }
    }

}