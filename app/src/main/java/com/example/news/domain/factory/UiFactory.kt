package com.example.news.domain.factory

import com.example.news.domain.entity.News
import com.example.news.ui.NewsUiDto

class UiFactory {

    fun createNewsUiDto(entity: News): NewsUiDto {

        val id = entity.newsLinksId
        val title = entity.title
        val text = entity.text
        val imageUrl = entity.imageUrl

        return NewsUiDto(id, title, text, imageUrl)
    }
}