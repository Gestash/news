package com.example.news.domain.entity

data class News(
    val newsLinksId: Int,
    val title: String,
    val text: String,
    val imageUrl: String
)