package com.example.news.domain.usecase

import com.example.news.domain.datasource.EndpointDataSource

class RefreshEndpointsUseCase(
    private val dataSource: EndpointDataSource
) {

    suspend fun refreshEndpoints(): Boolean {
        val endpoints = dataSource.getLatestEndpointsFromNet() ?: return false
        dataSource.saveEndpointsToDb(endpoints)
        return true
    }
}
