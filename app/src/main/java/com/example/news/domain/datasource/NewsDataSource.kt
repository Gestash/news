package com.example.news.domain.datasource

import com.example.news.db.repository.NewsRepository
import com.example.news.domain.entity.Endpoint
import com.example.news.domain.entity.News
import com.example.news.domain.factory.DbFactory
import com.example.news.domain.factory.NetFactory
import com.example.news.net.RestAPI

class NewsDataSource(
    private val restAPI: RestAPI,
    private val newsRepository: NewsRepository
) {

    private val netFactory = NetFactory()
    private val dbFactory = DbFactory()

    suspend fun getNewsFromDb(newsIds: List<Int>): List<News> {
        val newsDb = newsRepository.selectNewsFor(newsIds)
        return newsDb.map { dbFactory.createNews(it) }
    }

    suspend fun getLastNewsFromDb(limit:Int,offset:Int):List<News>{
        val dbDto = newsRepository.selectLastBatch(limit, offset)
        return dbDto.map { dbFactory.createNews(it) }
    }

    suspend fun saveNewsToDb(news: List<News>) {
        val newsDbDto = news.map { dbFactory.createNewsDbDto(it) }
        newsRepository.insertAll(newsDbDto)
    }

    suspend fun getNewsFromNet(endpoints: List<Endpoint>): List<News> {

        val result = ArrayList<News>()

        for (endpoint in endpoints) {
            val id = endpoint.id ?: continue
            val newsNetDto = restAPI.getNews(endpoint.endpoint) ?: continue
            val news = netFactory.createNews(id, newsNetDto)
            result.add(news)
        }

        return result
    }
}