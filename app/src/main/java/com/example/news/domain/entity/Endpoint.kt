package com.example.news.domain.entity

data class Endpoint(
    val id: Int?,
    val endpoint: String
)


