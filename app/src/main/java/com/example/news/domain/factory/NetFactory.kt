package com.example.news.domain.factory

import com.example.news.domain.entity.Endpoint
import com.example.news.domain.entity.News
import com.example.news.net.LatestNewsNetDto
import com.example.news.net.NewsNetDto

class NetFactory {

    fun createLatestNewsEndpoint(dto: LatestNewsNetDto): Endpoint {

        val endpoint = dto.info.id

        return Endpoint(null, endpoint)
    }

    fun createNews(newsLinksId: Int, dto: NewsNetDto): News {

        val title = dto.topic.headline.info.title
        val text = dto.topic.headline.info.announce
        val imageUrl = dto.topic.headline.titleImage.url

        return News(newsLinksId, title, text, imageUrl)
    }
}