package com.example.news.presenter

interface IMainPresenter {
    fun refreshEndpoints()
    fun getNews()
}