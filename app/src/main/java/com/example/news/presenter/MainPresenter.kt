package com.example.news.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.news.NewsApplication
import com.example.news.ui.NewsUiDto
import com.example.news.utils.NotificationHelper
import com.example.news.view.MainView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@InjectViewState
class MainPresenter : MvpPresenter<MainView>(), IMainPresenter {

    private val refreshEndpointsUseCase = NewsApplication.getComponent().refreshEndpointsUseCase()
    private val newsBatchUseCase = NewsApplication.getComponent().newsBatchUseCase()
    private val context = NewsApplication.getComponent().getContext()
    private val notificationHelper = NotificationHelper(context)

    private val bgScope = CoroutineScope(Dispatchers.Default)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    val news = arrayListOf<NewsUiDto>()
    private val bookmarks = arrayListOf<Int>()

    var success = false

    override fun refreshEndpoints() {
        bgScope.launch {
            success = refreshEndpointsUseCase.refreshEndpoints()

            uiScope.launch {
                news.clear()
                viewState.onEndpointsRefreshed()
            }
        }
    }

    override fun getNews() {

        bgScope.launch {
            val listNews = loadNews()
            uiScope.launch {
                news.addAll(listNews)
                viewState.onNewsUpdated()
            }
        }
    }

    private suspend fun loadNews(): List<NewsUiDto> {
        return if (success) {
            newsBatchUseCase.loadNewsBatch(news.count())
        } else {
            newsBatchUseCase.loadNewsBatchFromDb(news.count())
        }
    }

    fun toggleNewsToBookmark(id: Int): BookmarkStatus {
        return if (bookmarks.contains(id)) {
            bookmarks.remove(id)
            BookmarkStatus.REMOVED
        } else {
            bookmarks.add(id)
            BookmarkStatus.ADDED
        }
    }

    fun onStop() {
        val result = news.filter { bookmarks.contains(it.id) }
        result.map { notificationHelper.createNotification(it) }
        bookmarks.clear()
    }

    fun getNewsIndexById(id: Int): Int? {
        val element = news.find { it.id == id }
        checkNotNull(element) { return null }
        return news.indexOf(element)
    }
}

enum class BookmarkStatus {
    ADDED,
    REMOVED
}