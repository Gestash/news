package com.example.news.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.example.news.ui.MainActivity
import com.example.news.ui.NewsUiDto


class NotificationHelper(
    private val context: Context
) {

    private val channelId = "newsChannel"
    private val channelName = "news" // todo брать из ресурсов
    private val notificationManager: NotificationManager

    init {
        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= 26) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }
    }

    fun createNotification(newsUiDto: NewsUiDto) {
        val intent = Intent(context, MainActivity::class.java).apply {
            putExtra("id", newsUiDto.id)
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        }

        val pendingIntent = PendingIntent.getActivity(context, newsUiDto.id, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val options = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeResource(context.resources, com.example.news.R.mipmap.ic_launcher, options)

        val mBuilder = NotificationCompat.Builder(context, channelId).apply {
            setContentIntent(pendingIntent)
            setSmallIcon(com.example.news.R.mipmap.ic_launcher)
            setLargeIcon(bitmap)
            setContentTitle(newsUiDto.title)
            setContentText(newsUiDto.text)
            setAutoCancel(true)
        }

        notificationManager.notify(newsUiDto.id, mBuilder.build())
    }
}