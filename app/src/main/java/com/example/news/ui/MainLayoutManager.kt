package com.example.news.ui

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSmoothScroller
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Recycler
import android.support.v7.widget.RecyclerView.State


class MainLayoutManager(private val context: Context, orientation: Int, reverseLayout: Boolean) :
    LinearLayoutManager(context, orientation, reverseLayout) {
    var scaleDownBy = 0.2f

    override fun scrollVerticallyBy(dy: Int, recycler: Recycler?, state: State?): Int {

        if (this.orientation == RecyclerView.VERTICAL) {
            val scrolled = super.scrollVerticallyBy(dy, recycler, state)
            val mid = this.height / 2.0f

            for (i in 0 until this.childCount) {
                val child = this.getChildAt(i)

                val childMid = (this.getDecoratedTop(child) + this.getDecoratedBottom(child)).toFloat() / 2.0f

                val scale = 1.0f - this.scaleDownBy * Math.abs(mid - childMid) / mid

                child.scaleX = scale
                child.scaleY = scale

                child.pivotY =
                    if (mid < childMid) mid * 0.05f else mid * 1.5f // todo временное решение, переделать
            }
            return scrolled
        } else {
            return 0
        }
    }

    fun scrollTo(position: Int) {
        val smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return LinearSmoothScroller.SNAP_TO_START
            }
        }
        smoothScroller.targetPosition = position
        this.startSmoothScroll(smoothScroller)
    }
}
