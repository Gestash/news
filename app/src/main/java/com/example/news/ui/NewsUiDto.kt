package com.example.news.ui

data class NewsUiDto(
    val id: Int,
    val title: String,
    val text:String,
    val imageUrl: String
)