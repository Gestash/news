package com.example.news.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.news.R
import com.example.news.presenter.BookmarkStatus
import com.example.news.presenter.MainPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_item.view.*
import org.jetbrains.anko.sdk25.listeners.onClick

class MainViewAdapter(
    private val mainPresenter: MainPresenter,
    val context: Context
) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.news_item, parent, false)
        return ViewHolder(view, mainPresenter)
    }

    override fun getItemCount(): Int {
        return mainPresenter.news.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(mainPresenter.news[position])

        if (position == mainPresenter.news.count() - 1) {
            mainPresenter.getNews()
        }
    }
}

class ViewHolder(view: View, private val mainPresenter: MainPresenter) : RecyclerView.ViewHolder(view) {

    private val newsTitle = view.newsTitle
    private val newsText = view.newsText
    private val newsImage = view.newsImage
    private val imageBtn = view.imageBtn

    private var id: Int? = null

    init {
        imageBtn.onClick {
            val id = checkNotNull(id) { return@onClick }
            onBookmarkClick(id)
        }
    }

    fun bindItems(newsUiDto: NewsUiDto) {
        id = newsUiDto.id

        newsTitle.text = newsUiDto.title
        newsText.text = newsUiDto.text

        Picasso.get()
            .load(newsUiDto.imageUrl)
            .placeholder(R.drawable.place_holder)
            .into(newsImage)
    }

    private fun onBookmarkClick(id: Int) {
        val status = mainPresenter.toggleNewsToBookmark(id)
        val imageResourceId = when (status) {
            BookmarkStatus.ADDED -> android.R.drawable.btn_star_big_on
            BookmarkStatus.REMOVED -> android.R.drawable.btn_star_big_off
        }
        imageBtn.setImageResource(imageResourceId)
    }

}
