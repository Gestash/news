package com.example.news.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.PresenterType
import com.example.news.R
import com.example.news.presenter.MainPresenter
import com.example.news.view.MainView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter(type = PresenterType.GLOBAL)
    lateinit var mainPresenter: MainPresenter
    private lateinit var adapter: MainViewAdapter
    private lateinit var mainLayoutManager: MainLayoutManager

    private var id = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = MainViewAdapter(mainPresenter, this)
        mainLayoutManager = MainLayoutManager(this, RecyclerView.VERTICAL, false)

        recycler_view.layoutManager = mainLayoutManager
        recycler_view.adapter = adapter

        mainPresenter.refreshEndpoints()

        swipeContainer.setOnRefreshListener {
            mainPresenter.refreshEndpoints()
        }
    }

    override fun onStop() {
        super.onStop()
        mainPresenter.onStop()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        checkNotNull(intent) { return }

        id = intent.getIntExtra("id", -1)

        if (mainPresenter.news.isNotEmpty()) {
            scrollToNotificationNews()
        }
    }

    override fun onEndpointsRefreshed() {
        mainPresenter.getNews()
        swipeContainer.isRefreshing = false
    }

    override fun onNewsUpdated() {
        adapter.notifyDataSetChanged()

        scrollToNotificationNews()
    }

    private fun scrollToNotificationNews() {
        check(id != -1) { return }
        val position = mainPresenter.getNewsIndexById(id)
        checkNotNull(position) { return }
        mainLayoutManager.scrollTo(position)
        id = -1
    }
}


