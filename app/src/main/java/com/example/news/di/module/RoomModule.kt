package com.example.news.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.example.news.db.NewsDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun newsDataBase(context: Context): NewsDataBase {
        return Room.databaseBuilder(context,
            NewsDataBase::class.java,
            "news.db")
            .build()
    }

    @Provides
    @Singleton
    fun providesLatestEndpointsDataDao(database: NewsDataBase) = database.latestEndpointsDataDao()

    @Provides
    @Singleton
    fun providesNewsDao(database: NewsDataBase) = database.newsDataDao()

}