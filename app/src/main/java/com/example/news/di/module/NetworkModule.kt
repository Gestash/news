package com.example.news.di.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {
    private val LENTA_BASE_URL = "https://api.lenta.ru/"

    @Provides
    @Singleton
    fun retrofit(okhttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(LENTA_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okhttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun okhttpClient(): OkHttpClient {
        val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
            this.readTimeout(5, TimeUnit.SECONDS)
        }.build()
    }
}
