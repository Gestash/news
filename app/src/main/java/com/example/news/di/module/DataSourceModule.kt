package com.example.news.di.module

import com.example.news.db.repository.EndpointsRepository
import com.example.news.db.repository.NewsRepository
import com.example.news.domain.datasource.EndpointDataSource
import com.example.news.domain.datasource.NewsDataSource
import com.example.news.net.RestAPI
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DataSourceModule {

    @Provides
    @Singleton
    fun endpointDataSource(
        restAPI: RestAPI,
        endpointsRepository: EndpointsRepository
    ): EndpointDataSource{
        return EndpointDataSource(
            restAPI,
            endpointsRepository
        )
    }

    @Provides
    @Singleton
    fun newsDataSource(
        restAPI: RestAPI,
        newsRepository: NewsRepository
    ): NewsDataSource {
        return NewsDataSource(
            restAPI,
            newsRepository
        )
    }
}