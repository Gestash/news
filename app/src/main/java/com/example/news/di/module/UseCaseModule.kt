package com.example.news.di.module

import com.example.news.domain.datasource.EndpointDataSource
import com.example.news.domain.datasource.NewsDataSource
import com.example.news.domain.usecase.NewsBatchUseCase
import com.example.news.domain.usecase.RefreshEndpointsUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun refreshEndpointsUseCase(
        endpointDataSource: EndpointDataSource
    ): RefreshEndpointsUseCase {
        return RefreshEndpointsUseCase(endpointDataSource)
    }

    @Provides
    @Singleton
    fun newsBatchUseCase(
        endpointDataSource: EndpointDataSource,
        newsDataSource: NewsDataSource
    ): NewsBatchUseCase {
        return NewsBatchUseCase(
            endpointDataSource,
            newsDataSource
        )
    }
}