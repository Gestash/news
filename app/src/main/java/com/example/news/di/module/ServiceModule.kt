package com.example.news.di.module

import com.example.news.net.NewsService
import com.example.news.net.RestAPI
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class ServiceModule {

    @Provides
    @Singleton
    fun newsService(retrofit: Retrofit): NewsService {
        return retrofit.create(NewsService::class.java)
    }

    @Provides
    @Singleton
    fun restAPI(newsService: NewsService):RestAPI{
        return RestAPI(newsService)
    }
}