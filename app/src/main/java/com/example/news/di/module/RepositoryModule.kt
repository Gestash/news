package com.example.news.di.module

import com.example.news.db.repository.NewsRepository
import com.example.news.db.repository.EndpointsRepository
import com.example.news.db.dao.LatestEndpointsDataDao
import com.example.news.db.dao.NewsDataDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module

class RepositoryModule {

    @Provides
    @Singleton
    fun endpointsRepository(
        latestEndpointsDataDao: LatestEndpointsDataDao
    ): EndpointsRepository {
        return EndpointsRepository(
            latestEndpointsDataDao
        )
    }

    @Provides
    @Singleton
    fun newsRepository(
        newsDataDao: NewsDataDao
    ): NewsRepository {
        return NewsRepository(
            newsDataDao
        )
    }
}