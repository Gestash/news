package com.example.news.di

import android.content.Context
import com.example.news.di.module.*
import com.example.news.domain.usecase.NewsBatchUseCase
import com.example.news.domain.usecase.RefreshEndpointsUseCase
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ServiceModule::class,
        ContextModule::class,
        RoomModule::class,
        NetworkModule::class,
        UseCaseModule::class,
        RepositoryModule::class,
        DataSourceModule::class
    ]
)
@Singleton
interface SingletonComponent {
    fun refreshEndpointsUseCase(): RefreshEndpointsUseCase
    fun newsBatchUseCase(): NewsBatchUseCase
    fun getContext(): Context
}