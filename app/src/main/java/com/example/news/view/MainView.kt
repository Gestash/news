package com.example.news.view

import com.arellomobile.mvp.MvpView

interface MainView : MvpView {
    fun onEndpointsRefreshed()
    fun onNewsUpdated()
}