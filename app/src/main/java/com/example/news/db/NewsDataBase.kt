package com.example.news.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.news.db.dao.LatestEndpointsDataDao
import com.example.news.db.dao.NewsDataDao
import com.example.news.db.dto.NewsDbDto
import com.example.news.db.dto.NewsEndpointDbDto

@Database(
    entities = [
        NewsEndpointDbDto::class,
        NewsDbDto::class
    ],
    version = 1,
    exportSchema = false
)
abstract class NewsDataBase : RoomDatabase() {

    abstract fun newsDataDao(): NewsDataDao
    abstract fun latestEndpointsDataDao(): LatestEndpointsDataDao
}