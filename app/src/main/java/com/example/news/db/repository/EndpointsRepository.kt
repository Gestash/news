package com.example.news.db.repository

import com.example.news.db.dao.LatestEndpointsDataDao
import com.example.news.db.dto.NewsEndpointDbDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class EndpointsRepository(
    private val latestEndpointsDataDao: LatestEndpointsDataDao
) {

    suspend fun insertAll(endpoints: List<NewsEndpointDbDto>) =
        withContext(Dispatchers.IO) {
            try {
                latestEndpointsDataDao.insertAll(endpoints)
            } catch (e: Exception) {
                //ignore unique constraint exception
            }
        }

    suspend fun selectLastBatch(limit: Int, offset: Int): List<NewsEndpointDbDto> =
        withContext(Dispatchers.IO) {
            latestEndpointsDataDao.selectLastBatch(limit, offset)
        }
}
