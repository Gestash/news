package com.example.news.db.dto

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey


@Entity(
    tableName = "news_links",
    indices = [
        Index(
            value = ["endpoint"],
            unique = true
        )
    ]
)
data class NewsEndpointDbDto(

    val endpoint: String
)
{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}