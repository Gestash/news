package com.example.news.db.dto

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(
    tableName = "news",
    indices = [
        android.arch.persistence.room.Index(
            value = ["news_links_id"],
            unique = true
        )
    ]
)
data class NewsDbDto(

    @ColumnInfo(name = "news_links_id")
    var newsLinksId: Int,
    val title: String,
    val text: String,

    @ColumnInfo(name = "image_url")
    val imageUrl: String
)
{
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}