package com.example.news.db.repository

import com.example.news.db.dao.NewsDataDao
import com.example.news.db.dto.NewsDbDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NewsRepository(private val newsDataDao: NewsDataDao) {

    suspend fun insertAll(newsDbDto: List<NewsDbDto>) =
        withContext(Dispatchers.IO) {

            try {
                newsDataDao.insertAll(newsDbDto)
            } catch (e: Exception) {
                //ignore unique constraint exception
            }

        }

    suspend fun selectNewsFor(ids: List<Int>): List<NewsDbDto> =
        withContext(Dispatchers.IO) {
            newsDataDao.selectNewsFor(ids)
        }

    suspend fun selectLastBatch(limit: Int, offset: Int): List<NewsDbDto> =
        withContext(Dispatchers.IO) {
            newsDataDao.selectLastBatch(limit, offset)
        }
}
