package com.example.news.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.news.db.dto.NewsEndpointDbDto

@Dao
interface LatestEndpointsDataDao {
    @Insert
    fun insertAll(endpoints: List<NewsEndpointDbDto>)

    @Query("SELECT * FROM news_links ORDER BY id DESC LIMIT :limit OFFSET :offset")
    fun selectLastBatch(limit: Int, offset: Int): List<NewsEndpointDbDto>
}