package com.example.news.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.news.db.dto.NewsDbDto

@Dao
interface NewsDataDao {

    @Insert
    fun insertAll(newsDbDto: List<NewsDbDto>)

    @Query("SELECT * FROM news WHERE news_links_id IN (:ids)")
    fun selectNewsFor(ids: List<Int>):List<NewsDbDto>

    @Query("SELECT * FROM news ORDER BY news_links_id DESC LIMIT :limit OFFSET :offset")
    fun selectLastBatch(limit: Int, offset: Int):List<NewsDbDto>

}